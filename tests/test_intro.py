from sample import intro


def test_pal_lower_than():
    assert len(list(intro.pal_lower_than(123))) == 22
    assert len(list(intro.pal_lower_than(400))) == 49


def test_is_palindrome():
    assert intro.is_palindrome(0) == True
    assert intro.is_palindrome(112211) == True
    print(intro.is_palindrome(112211))
    assert intro.is_palindrome(123) == False


def test_upper_reversed():
    assert intro.upper_reversed('aaa') == 'AAA'
    assert intro.upper_reversed('fAb') == 'BAF'
    assert intro.upper_reversed('AbCd') == 'DCBA'


def test_upper_reversed2():
    assert intro.upper_reversed2('aaa') == 'AAA'
    assert intro.upper_reversed2('fAb') == 'BAF'
    assert intro.upper_reversed2('AbCd') == 'DCBA'


def test_is_text_palindrome():
    assert intro.is_text_palindrome('a, bb, a') == True
    assert intro.is_text_palindrome('abcba') == True
    assert intro.is_text_palindrome(',,,aba') == True
    assert intro.is_text_palindrome('   abaa   ') == False
    assert intro.is_text_palindrome('abc') == False
    assert intro.is_text_palindrome(',..a,,,,b,c,.') == False
    assert intro.is_text_palindrome(',..a,,,,b,a,.') == True
    assert intro.is_text_palindrome('') == True
    assert intro.is_text_palindrome(',A,,,,A,,,A') == True
    assert intro.is_text_palindrome('A ,.,.,.San,.,.,ta,.,., at Na.,sa..as,.,aN ta,.,., atnaS A') == True
