from sample import funcs


def test_merge():
    l = [1, 3, 5, 7]
    l2 = [2, 4, 6, 8, 10, 12, 14]
    assert funcs.merge(l, l2) == [1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 14]

    l = range(1, 20)
    l2 = range(20, 50)
    assert funcs.merge(l, l2) == list(range(1, 50))

    l = range(1, 30)
    l2 = range(30, 35)
    assert funcs.merge(l, l2) == list(range(1, 35))

    l = []
    l2 = range(1, 40)
    assert funcs.merge(l, l2) == list(range(1, 40))


def test_merge_sort():
    assert funcs.merge_sort(list(range(100, 0, -1))) == list(range(1, 101))
    assert funcs.merge_sort([10, 20, 1, 2, 4]) == [1, 2, 4, 10, 20]
    assert funcs.merge_sort(list(range(50, 0, -2))) == list(range(2, 51, 2))


def test_binary():
    assert funcs.binary_search(range(1, 100), 50) == True
    assert funcs.binary_search(range(1, 100), 1) == True
    assert funcs.binary_search(range(1, 100), 400) == False


def test_mymap():
    assert funcs.mymap(lambda x: x * 2, [1, 2, 3]) == [2, 4, 6]
    assert funcs.mymap(lambda x: x ** 2, [4, 10, 2]) == [16, 100, 4]


def test_myfilter():
    assert funcs.myfilter(lambda x: True if x % 2 == 0 else False, range(10)) == list(range(0, 10, 2))


def test_myreduce():
    assert funcs.myreduce(lambda x, y: x + y, [1, 2, 3], 0) == 6
    assert funcs.myreduce(lambda x, y: x * y, [1, 2, 3, 4], 1) == 24


def test_fib_iterativ():
    assert funcs.fib_iterativ(10) == [1, 1, 2, 3, 5, 8]
    assert len(funcs.fib_iterativ(1000)) == 16


def test_fib_recurs():
    assert funcs.fib_recurs(10) == [1, 1, 2, 3, 5, 8]
    assert len(funcs.fib_recurs(1000)) == 16
    assert funcs.fib_iterativ(10 ** 4) == funcs.fib_recurs(10 ** 4)


def test_prime_lower():
    assert len(list(funcs.prime_lower_than(1000))) == 168
    assert len(list(funcs.prime_lower_than(0))) == 0
    assert len(list(funcs.prime_lower_than(-2))) == 0
    assert len(list(funcs.prime_lower_than(12))) == 5


def test_is_prime():
    assert funcs.is_prime(-5) == False
    assert funcs.is_prime(10) == False
    assert funcs.is_prime(1) == False
    assert funcs.is_prime(2) == True
    assert funcs.is_prime(3) == True
    assert funcs.is_prime(4) == False
    assert funcs.is_prime(13) == True