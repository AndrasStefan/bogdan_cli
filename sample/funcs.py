import functools
import time


def memoization(func):
    values = {}

    def wrap(*args, **kwargs):
        print("IN MEMO")
        if args not in values:
            values[args] = func(*args, **kwargs)

        return values[args]

    return wrap


def measure_time(func, p1, p2):
    if p1 > p2:
        print("A LA BALA")

    @functools.wraps(func)
    def wrap(*args, **kwargs):
        start = time.time()

        ret = func(*args, **kwargs)

        end = time.time()
        print(end - start)

        return ret

    return wrap


def measure_total_time(_l=[]):
    print("AM INTRAT AICI")
    l = _l

    def deco(func):
        @functools.wraps(func)
        def wrap(*args):
            print("IN TOTAL TIME")
            start = time.time()
            ret = func(*args)
            end = time.time()
            t = end - start
            l.append( (func.__name__, t) )
            total = 0
            print
            for elem in l:
                print(str(elem[0]) + ' took: ' + str(format(elem[1], '.5f')))
                total += elem[1]
            print('Total time: ' + str(format(total, '.5f')))
            print
            return ret

        return wrap

    return deco


def is_prime(n):
    if n <= 0:
        return False
    elif n == 1:
        return False
    elif n == 2:
        return True
    else:
        for d in range(2, int(n ** 0.5) + 1):
            if n % d == 0:
                return False
    return True


def fib_iterativ(n):
    t1, t2 = 0, 1
    rez = []
    while t2 < n:
        rez.append(t2)
        t1, t2 = t2, t1 + t2
    return rez


def fib_recurs(n, t1=0, t2=1):
    if t1 + t2 > n:
        return [t2]

    return [t2] + fib_recurs(n, t1=t2, t2=t1 + t2)


def mymap(func, iterable):
    return [func(elem) for elem in iterable]


def myfilter(func, iterable):
    return [elem for elem in iterable if func(elem)]


def myreduce(func, iterable, initializer):
    last = initializer
    for elem in iterable:
        curent = func(last, elem)
        last = curent
    return last


def special_sum(n):
    return myreduce(lambda x, y: x + y, myfilter(lambda x: True if x % 3 == 0 else False, mymap(lambda x: x * x - 1, xrange(n + 1))), 0)


def merge(a, b):
    i, j = 0, 0
    rez = []
    while i < len(a) and j < len(b):
        if a[i] < b[j]:
            rez.append(a[i])
            i += 1
        else:
            rez.append(b[j])
            j += 1

    rez += a[i:]
    rez += b[j:]
    return rez


def merge_sort(l):
    if len(l) == 1:
        return l

    mid = len(l) // 2

    stg = merge_sort(l[:mid])
    dr = merge_sort(l[mid:])

    return merge(stg, dr)


def binary_search(l, value):
    st = 0
    dr = len(l) - 1
    found = 0

    while st <= dr and not found:
        mid = (st + dr) // 2
        if l[mid] == value:
            found = True
        elif value < l[mid]:
            dr = mid - 1
        else:
            st = mid + 1

    return found


def counter(l = [0]):
    l[0] += 1
    return l[0]


def prime_lower_than(n):
    return filter(is_prime, range(n + 1))


def file_reader(file, size = 1024):
    rez = file.read(size)
    while rez:
        yield rez
        rez = file.read(size)


class reader():
    def __init__(self, f):
        self.f = f

    def send(self, size = 1024):
        rez = self.f.read(size)
        while rez:
            yield rez
            rez = self.f.read(size)