import click
from . import funcs


@click.command()
@click.option('--number', default=10,
            help='Give a number')
def fibo(number):
    rez = funcs.fib_iterativ(number)
    click.echo(rez)

if __name__ == '__main__':
    fibo()




