def upper_reversed(s):
    return s[::-1].upper()


def upper_reversed2(s):
    rez = ''
    for i in range(len(s) - 1, -1, -1):
        if s[i] >= 'a' and s[i] <= 'z':
            rez += chr(ord(s[i]) - 32)
        else:
            rez += s[i]
    return rez


def is_palindrome(n):
    aux = n
    o = 0
    while aux:
        o = o * 10 + aux % 10
        aux //= 10
    return o == n


def is_text_palindrome(text):
    lung = len(text)

    st, dr = 0, lung - 1
    while st <= dr:
        while not text[st].isalpha():
            st += 1
        while not text[dr].isalpha():
            dr -= 1

        if text[st] != text[dr]:
            return False

        st += 1
        dr -= 1

    return True


def pal_lower_than(n):
    return filter(is_palindrome, range(n + 1))
