from setuptools import setup

setup(
    name='sample',
    version='1.0',
    py_module=['ui'],
    install_requires=[
        'Click',
    ],
)
